#include <stdio.h>

int main(){

int tamanio;
int posicion;
int opciones=0;

int disponible=0;

int datoIngresado;
int posicionIngreso;
int datoBuscar;
int encontrar=0;
int editar;
int posicionEditar;
int posicionEliminar;

	printf("Ingrese el tamanio del vector\n");
	scanf("%d",&tamanio);
	
	int vector[tamanio];
	
	for(posicion=0;posicion<tamanio;posicion++){
		vector[posicion]=0;
	}
	

do{
	printf("\n--------------------------Menu----------------------------------\n");
	printf("Ingrese un numero valido para acceder a las opciones del menu\n");
	printf("1-Agresar un dato a la posicion de la vector\n");
	printf("2-Buscar un valor en el vector\n");
	printf("3-Editar\n");
	printf("4-Eliminar\n");
	printf("cualquier otro numero para salir\n");
	printf("-----------------------------------------------------------------\n");
	scanf("%d",&opciones);
	
	if(opciones==1){
		
		printf("Posiciones disponibles del vector\n");
		
		for(posicion=0;posicion<tamanio;posicion++){
			if(vector[posicion]==0){
				printf("%d",posicion+1);
				printf("\t");
				disponible=disponible+1;
			 }else {
				printf("X");
				printf("\t");
			}
		}
		
		if(disponible!=0){
			
			do{
			printf("\nIngrese la posicion donde quiere ingresar el dato diferente de 0 y cuya posicion este disponible\n");
			scanf("%d",&posicionIngreso);
		    }while(posicionIngreso==0||vector[posicionIngreso-1]!=0);	
		    
		    printf("Ingrese el numero que quiere añadir a la posicion %d",posicionIngreso);
		    printf("\n");
		    scanf("%d",&datoIngresado);
		    
		    vector[posicionIngreso-1]=datoIngresado;
		    disponible=0;
		    
		}else {
			printf("\nNo hay posiciones disponibles\n");
			}
	
		
	}else if(opciones==2){
		
		printf("Ingrese el valor que desea buscar en el vector\n");
		scanf("%d",&datoBuscar);
		
		for(posicion=0;posicion<tamanio;posicion++){
			if(datoBuscar==vector[posicion]){
				printf("Se encontro el dato %d",datoBuscar);
				printf(" en la posicion %d",posicion+1);
				printf("\n");
				encontrar=encontrar+1;
			}
		} 
			
			printf("\nSe encontro %d",encontrar);
			printf(" veces el dato en el vector \n");
			encontrar=0;
	
	}else if(opciones==3){
		
		printf("\nIngrese la posicion del dato que desea editar\n");
		for(posicion=0;posicion<tamanio;posicion++){
			printf("Posicion %d",posicion+1);
			printf(" = %d",vector[posicion]);
			printf("\t");
		}
		printf("\n");
		
		scanf("%d",&posicionEditar);
		printf("\nIngrese el dato que desea editar\n");
		scanf("%d",&editar);
		
		vector[posicionEditar-1]=editar;
		
		for(posicion=0;posicion<tamanio;posicion++){
			printf("%d",vector[posicion]);
			printf("\t");
		}
		
	}else if(opciones==4){
	
	  printf("\nIngrese la posicion del dato que desea eliminar\n");
		for(posicion=0;posicion<tamanio;posicion++){
			printf("Posicion %d",posicion+1);
			printf(" = %d",vector[posicion]);
			printf("\t");
		}
		printf("\n");
		
		scanf("%d",&posicionEliminar);
		
		vector[posicionEliminar-1]=0;
		
		for(posicion=0;posicion<tamanio;posicion++){
			printf("%d",vector[posicion]);
			printf("\t");
		}
	
	}
	
}while(opciones>0&&opciones<5);

	return 0;
}
