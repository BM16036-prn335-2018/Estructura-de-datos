#include <stdio.h>

int main(){
int dimencion;
int fila,columna;

printf("Ingrese la dimencion de la matriz A \n");
scanf("%d",&dimencion);

int matrizA[dimencion][dimencion];
int matrizB[dimencion][dimencion];

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("Ingrese el dato del matriz A fila: %d",fila+1);
		printf(", columna: %d",columna+1);
		printf("\n");
		scanf("%d",&matrizA[fila][columna]);
	}
}

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("%d",matrizA[fila][columna]);
			printf("\t");
	}
	printf("\n");
}

printf("-----------------------------------\n");

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		matrizB[columna][fila]=matrizA[fila][columna];
	}
}

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("%d",matrizB[fila][columna]);
			printf("\t");
	}
	printf("\n");
}
	return 0;
}
