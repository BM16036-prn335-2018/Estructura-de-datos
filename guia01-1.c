#include <stdio.h>

int main(){
int dimencion;
int fila,columna;
int repeticiones;
int sumaR=0;

printf("Ingrese la dimencion de la matriz A \n");
scanf("%d",&dimencion);

int matrizA[dimencion][dimencion];
int matrizB[dimencion][dimencion];
int matrizR[dimencion][dimencion];


printf("ingrese los datos de la matriz A\n");
for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("Ingrese el dato del matriz A fila: %d",fila+1);
		printf(", columna: %d",columna+1);
		printf("\n");
		scanf("%d",&matrizA[fila][columna]);
	}
}

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("%d",matrizA[fila][columna]);
			printf("\t");
	}
	printf("\n");
}

printf("-----------------------------------\n");

printf("ingrese los datos de la matriz B\n");
for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("Ingrese el dato del matriz B fila: %d",fila+1);
		printf(", columna: %d",columna+1);
		printf("\n");
		scanf("%d",&matrizB[fila][columna]);
	}
}

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("%d",matrizB[fila][columna]);
			printf("\t");
	}
	printf("\n");
}
printf("-----------------------------------\n");


for(repeticiones=0;repeticiones<dimencion;repeticiones++){
	for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		sumaR=sumaR+(matrizA[repeticiones][columna]*matrizB[columna][fila]);	
	}
	matrizR[repeticiones][fila]=sumaR;
 }
}

for(fila=0;fila<dimencion;fila++){
	for(columna=0;columna<dimencion;columna++){
		printf("%d",matrizR[fila][columna]);
			printf("\t");
	}
	printf("\n");
}

	return 0;
}
